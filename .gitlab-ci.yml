default:
  image:
    # This includes NPM and semantic-release dependencies installed.
    name: registry.gitlab.com/crossref/infrastructure/aws-ecr-ecs-cicd-docker:latest

stages:
  - get-version
  - compile
  - deploy-stg
  - deploy-prd

# Calculate the semantic version for everything on the main branch
# (it will be released later). If the semantic-release tool decides that
# this commit doesn't warrant a release (e.g. changes are too minor)
# then the VERSION.txt file will not be created.
get-semantic-version:
  stage: get-version
  rules:
    # Only tag from main branch.
    - if: '$CI_COMMIT_BRANCH == "main"'
  script:
    - echo "Using semantic version"
    - npx semantic-release --dry-run
  artifacts:
    paths:
      - VERSION.txt

# Any other branch, generate a build number based on the build number.
get-generic-version:
  stage: get-version
  rules:
    # Only main is releasable.
    - if: '$CI_COMMIT_BRANCH != "main"'
  script:
    - echo "Using generic version"
    # Percolator expects major.minor.build
    - echo 0.build-$CI_PIPELINE_ID.0 > VERSION.txt
  artifacts:
    paths:
      - VERSION.txt

# Test and compile in one go, so that the compiled output is exactly
# the same as what was tested.
test-compile:
  image: clojure:temurin-17-lein-alpine
  stage: compile

  cache:
    paths:
      # Corresponds to project.clj's :local-repo setting.
      - .local-m2

  # This job will run whenever the VERSION was calculated, i.e.:
  # - every commit to a master branch that triggered a semantic-version change, or
  # - every commit to a non-master branch.
  before_script:
    - export APP_VERSION="$(cat VERSION.txt)"
    - if [ -z "$APP_VERSION" ]; then echo "APP_VERSION is not defined, exiting."; exit 0; fi
    - echo "APP_VERSION is ${APP_VERSION}"

  script:
    # Rewrite project.clj with version number.
    - lein change version set "\"${APP_VERSION}\""
    - lein deps
    - lein compile
    - lein uberjar

  artifacts:
    paths:
      # All that matters at this point is the uberjar.
      - target/
      - VERSION.txt

# Build image and deploy to staging
deploy-staging:
  image: registry.gitlab.com/crossref/infrastructure/aws-ecr-ecs-cicd-docker:latest

  dependencies:
    - test-compile

  rules:
    # Only deploy from main, and only if the semantic-release run deemed that this was releasable
    # and therefore produced a VERSION.txt file.
    - if: '$CI_COMMIT_BRANCH == "main"'

  stage: deploy-stg
  variables:
    AWS_DEFAULT_PROFILE: staging
  tags:
    - aws
    - crossref-portal
  services:
    - docker:dind
  before_script:
    - export APP_VERSION="$(cat VERSION.txt)"
    - echo "APP_VERSION is ${APP_VERSION}"

    # If this wasn't provided my get-semantic-version, then it's not worth a release.
    - if [ -z "$APP_VERSION" ]; then echo "APP_VERSION is not defined, exiting."; exit 0; fi

    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - npx semantic-release

    # Build as version-tagged
    - docker build --pull -t "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME" -t "$CI_REGISTRY_IMAGE:$APP_VERSION" .

    # Also make a 'latest' tag.
    - docker image tag "$CI_REGISTRY_IMAGE:$APP_VERSION" "$CI_REGISTRY_IMAGE:latest"

    # Tag the image both with the current release and the 'latest' tag for developer use.
    - docker push "$CI_REGISTRY_IMAGE:$APP_VERSION"
    - docker push "$CI_REGISTRY_IMAGE:latest"

  artifacts:
    paths:
      - CHANGELOG.md

## Optionally deploy image to production
deploy-production:
  dependencies:
    - test-compile
  image: registry.gitlab.com/crossref/infrastructure/aws-ecr-ecs-cicd-docker:latest
  stage: deploy-prd

  # Same criteria as deploy-staging.
  rules:
    - if: '$CI_COMMIT_BRANCH == "main"'

  variables:
    AWS_DEFAULT_PROFILE: production
  tags:
    - aws
    - crossref-portal
  services:
    - docker:dind
  before_script:
    - export APP_VERSION="$(cat VERSION.txt)"
    - echo "APP_VERSION is ${APP_VERSION}"

    # If this wasn't provided my get-semantic-version, then it's not worth a release.
    - if [ -z "$APP_VERSION" ]; then echo "APP_VERSION is not defined, exiting."; exit 0; fi

    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    echo "Done"