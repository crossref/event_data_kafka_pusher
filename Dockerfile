# Event Data Kafka Pusher

FROM clojure:temurin-17-lein-alpine

COPY target/uberjar/*-standalone.jar ./kafka-pusher.jar

ENTRYPOINT ["java", "-jar", "kafka-pusher.jar"]